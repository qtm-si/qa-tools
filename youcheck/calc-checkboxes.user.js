// ==UserScript==
// @name         Calculate Checkboxes
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Calculate checkboxes on an issue page in youtrack
// @author       You
// @match        https://*.myjetbrains.com/youtrack/issue*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        GM_addStyle
// @run-at document-end
// ==/UserScript==

(function() {
    'use strict';

	// add styles for your script
	//
	var styles = '.mytest { \
		color: grey; \
		background-color: #f7f9fa; \
		position: fixed; \
  		bottom: 10px; \
  		right: 10px; \
  		width: 300px; \
  		border: 1px solid #73AD21;	\
	';

	GM_addStyle(styles);

    // show a popup with fixed position
	//
	var content = '\
		<div class="mytest">\
			<table> \
				<tr> \
					<td>Unchecked:</td> \
					<td id="unchecked_value"></td> \
				</tr> \
				<tr> \
					<td>Last Number:</td> \
					<td id="last_value"></td> \
				</tr> \
			</table> \
		</div> \
	';

	setTimeout(function() {
		$("body").append(content);
		unsafeWindow.reloadCheckboxes();
		
		setInterval(function() {
			unsafeWindow.reloadCheckboxes();
		}, 3000);
		
	}, 3000);

	unsafeWindow.reloadCheckboxes = function() {
		var unchecked = $('.markdown-checkbox-list [type=checkbox]:not(:checked)').length;
		
		var max_number = 0;
		
		$(':checkbox').each(function() {
			if (this.nextSibling && this.nextSibling.nodeValue) {

		 		var match = this.nextSibling.nodeValue.match(/\s*(\d*)/)
				if (match && match[1]) {
					if (match[1] > max_number) {
						max_number = match[1];
					}
				}

			}
		})
		
		$('#unchecked_value').text(unchecked);
		$('#last_value').text(max_number);
	}

})();